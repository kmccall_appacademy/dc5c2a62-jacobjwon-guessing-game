# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  guess_count = 0
  won = false
  computer_num = rand(1..100)
  human_guess = -1

  while !won
    puts "Guess a number:"
    human_guess = gets.chomp.to_i
    puts human_guess
    guess_count += 1
    if human_guess == computer_num
      won = true
    else
      message = human_guess > computer_num ? "too high" : "too low"
      puts message
    end
  end
  puts "You win! Your guess is : #{human_guess}"
  puts "Number of guess: #{guess_count}"
end


def file_shuffler
  puts "What is your file name?"
  file_name = gets.chomp
  contents = File.readlines(file_name)
  contents.shuffle
  contents.each do |line|
    File.open(file_name+"-shuffled.txt","a") do |f|
      f.puts line
    end
  end

end
